PROJECT INFO
=====================
TASK MANAGER

DEVELOPER INFO
=====================
**NAME:** Vsevolod Zorin

**E-MAIL:** <seva89423@gmail.com>

STACK OF TECHNOLOGIES
=====================
- TypeScript
- Nest.js
- TypeORM
- PostgreSQL
- Git Bash 
- PUG

SOFTWARE
=====================
- Microsoft Visual Studio Code
- MS WINDOWS 10

PROGRAM BUILD
=====================
```
npm install
```

PROGRAM RUN
=====================
```
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
UPDATE LOGS
=====================
04.10 - 07.10 - отладка работы сущности Пользователя, а также добавление связи таблицы tasks и users
На данный момент ведётся работа по прокидыванию авторизации, пока получается не очень, но на следующей неделе будет сделано
