import { Injectable } from '@nestjs/common';
import { In } from 'typeorm';
import { User } from '../entity/user.model';

@Injectable()
export class UserRepository 
{
    async createUser(login: string, password: string)
    {
        let newUser = new User(login, password);
        return newUser.save();
    }

    async deleteUserByLogin(login: string)
    {
      let userToDelete = await User.findOne({ login: In([login])});
      if (userToDelete == undefined) {
        console.log("User does not exist");
        return;
      }
      await User.remove(userToDelete);
    }

    async deleteUserById(id: number)
    {
      let userToDelete = await User.findOne(id);
      if (userToDelete == undefined) {
        console.log("User does not exist");
        return;
      }
      await User.remove(userToDelete);
    }

    async updateUserByLogin(login: string, newLogin: string, password: string)
    {
      let userToUpdate = await User.findOne({ login: In([login])});
      if (userToUpdate == undefined) {
        console.log("User does not exist");
        return;
      }
      userToUpdate.login = newLogin;
      userToUpdate.password = password;
      await User.save(userToUpdate);
      console.log("Changing was succsessful!");
    }

    async updateUserById(id: number, newLogin: string, password: string)
    {
      let userToUpdate = await User.findOne(id);
      if (userToUpdate == undefined) {
        console.log("User does not exist");
        return;
      }
      userToUpdate.login = newLogin;
      userToUpdate.password = password;
      await User.save(userToUpdate);
    }

    async userLogin(userlogin:string, userPassword: string)
    {
      let userToLogin = await User.findOne({where: {login: userlogin, password: userPassword}});
      if (userToLogin == undefined) {
        console.log("User does not exist");
        return;
      }
      console.log("Auth was succsessful!");
    }
}