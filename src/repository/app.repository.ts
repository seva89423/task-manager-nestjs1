import { Injectable } from '@nestjs/common';
import { User } from 'src/entity/user.model';
import { In } from 'typeorm';
import { Task } from '../entity/task.model';

@Injectable()
export class AppRepository {

  async createTask(userID: number, name: string, description: string)
  {
    if (userID == null || userID == undefined)
    {
      console.log("Вы не авторизированы");
      return;
    }
    const newTask = new Task(name, description, userID);
    await newTask.save();
  }

  async updateTask(oldName: string, newName: string, newDescription: string)
  {
    let taskFind = await Task.findOne({ taskName: In([oldName])});
    if (taskFind == undefined) {
      console.log('Task does not exist');
      return;    
    }
    taskFind.taskName = newName;
    taskFind.taskDescription = newDescription;
    await Task.getRepository().save(taskFind);
  }

  async deleteTask(name: string)
  {
    let taskToDelete= await Task.findOne({ taskName: In([name])});
    if (taskToDelete == undefined) {
      console.log('Task does not exist');
      return;    
    }
    await Task.getRepository().remove(taskToDelete);
  }

  async getTasks()
  {
    let tasks = await Task.getRepository().find();
    return tasks;
  }

  async getTasksById(id:number)
  {
    const tasks = await Task.findOne(id);
    if (id == null || id == undefined)
    {
      console.log("Вы не авторизированы");
      return;
    }
    console.log(tasks);
    return tasks;
  }
}