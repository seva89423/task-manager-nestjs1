import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { AppModule } from './app.module';
import * as readline from 'readline';
import { AppService } from './service/app.service';
import { User } from './entity/user.model';
import { AppRepository } from './repository/app.repository';
import { UserService } from './service/user.service';
import { UserRepository } from './repository/user.repository';

async function main1()
{
  const appRepository = new UserRepository();
  console.log("Welcome to Task Manager\n");
  var user: User = new User('User', 'qwerty', 1);
  console.log(user);
  appRepository.userLogin(user.login, user.password);
  let userLogin = "user2";
  let userPassword = "qwerty2";
  appRepository.updateUserByLogin(user.login, userLogin, userPassword);
}

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.setBaseViewsDir(join(__dirname, '../views'));
  app.setViewEngine('pug');
  await app.listen(3000);
  //main1();
}

bootstrap();