import { Injectable } from '@nestjs/common';
import { AppRepository } from '../repository/app.repository';

@Injectable()
export class AppService {

constructor(private readonly repository: AppRepository) {}

  async createTask(userID:number, name: string, description: string)
  {
    return this.repository.createTask(userID,name, description);
  }

  async updateTask(oldName: string, newName: string, newDescription: string)
  {
    return this.repository.updateTask(oldName, newName, newDescription);
  }

  async deleteTask(name: string)
  {
    return this.repository.deleteTask(name);
  }

  async getTasks()
  {
    return this.repository.getTasks();
  }

  async getTasksById(id:number)
  {
    return this.repository.getTasksById(id);
  }
}