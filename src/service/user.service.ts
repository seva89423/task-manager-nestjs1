import { Injectable } from '@nestjs/common';
import { UserRepository } from '../repository/user.repository';

@Injectable()
export class UserService {

constructor(private readonly repository: UserRepository) {}

async createUser(login: string, password: string)
{
    return this.repository.createUser(login, password);
}

async deleteUserByLogin(login: string)
{
    return this.repository.deleteUserByLogin(login);
}

async deleteUserById(id: number)
{
    return this.repository.deleteUserById(id);
}

async updateUserByLogin(login: string, newLogin: string, password: string)
{
    return this.repository.updateUserByLogin(login, newLogin, password);
}

async updateUserById(id: number, newLogin: string, password: string)
{ 
    return this.repository.updateUserById(id, newLogin, password);
}

async userLogin(userlogin:string, userPassword: string)
{
    return this.repository.userLogin(userlogin, userPassword);
}
}