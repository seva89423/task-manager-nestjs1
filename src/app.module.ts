import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './controller/app.controller';
import { AppRepository } from './repository/app.repository';
import { AppService } from './service/app.service';
import { Task } from './entity/task.model';
import { User } from './entity/user.model';
import { UserService } from './service/user.service';
import { UserRepository } from './repository/user.repository';

@Module({
  imports: [ TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'test',
    password: 'test',
    database: 'NoSQLDB',
    logging: true,
    synchronize: true,
    entities: [Task, User],
  })],
  controllers: [AppController], 
  providers: [AppService, AppRepository, UserService, UserRepository],
})
export class AppModule {}
