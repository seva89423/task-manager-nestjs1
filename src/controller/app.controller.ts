import { Body, Controller, Get, Param, ParseIntPipe, Post, Put, Query, Redirect, Render } from '@nestjs/common';
import { User } from 'src/entity/user.model';
import { UserService } from 'src/service/user.service';
import { AppService } from '../service/app.service';

@Controller()
export class AppController {

  constructor(private readonly service: AppService, private readonly userService: UserService) {}

  @Get()
  @Render('index')
  async index() {
    return { 
      tasks: await this.service.getTasks()
    };
  }

  @Get('create')
  @Render('create-task')
  getFormCreate(): void {
    return;
  }

  @Get('update')
  @Render('update-task')
  getFormUpdate(): void {
    return;
  }

  @Get('delete')
  @Render('delete-task')
  getFormDelete(): void {
    return;
  }

  @Get('login')
  @Render('user-login')
  getFormLogin(): void {
    return;
  }

  @Post('tasks')
  @Redirect('/', 301)
  create(@Body() body: any): Promise<void> 
  {
    let userToCreate = User.getRepository().findOne(body.id);
    if (userToCreate == undefined)
    {
      console.log("User not found");
      return;
    }
    return this.service.createTask(body.id, body.taskName, body.taskDescription);
  }

  @Get('tasks')
  @Redirect('/', 301)
  delete(@Query() body: any): Promise<void> 
  {
    return this.service.deleteTask(body.taskName);
  }

  @Put('tasks')
  @Redirect('/', 301)
  update(@Query() body: any): Promise<void> 
  {
    return this.service.updateTask(body.oldName, body.taskName, body.taskDescription);
  }

  @Get('tasks/:id')
  @Render('task')
  async getById(@Param('id', ParseIntPipe) id: number) {
    return this.service.getTasksById(id);
  }
}