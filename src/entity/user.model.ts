import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'users' })
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    login: string;

  
    @Column()
    password: string;

    constructor(login: string, password: string, id?: number, userId?:number) {
        super();
        this.id = id;
        this.login = login;
        this.password = password;
      }
}  