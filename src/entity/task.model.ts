import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'tasks' })
export class Task extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  taskName: string;

  @Column()
  taskDescription: string;

  constructor(taskName: string, taskDescription: string, id?: number) {
    super();
    this.id = id;
    this.taskName = taskName;
    this.taskDescription = taskDescription;
  }
}